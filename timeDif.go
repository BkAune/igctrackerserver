package main

import "time"
import "math"
import "strconv"

//StartTime stores server start time
var StartTime time.Time

//Start sets StartTime value
func Start() {
	StartTime = time.Now()
}

//ISO8601 returns server uptime in ISO8601 format
func ISO8601() string {
	minutes, seconds := timeMod(time.Since(StartTime).Seconds(), 60)
	hours, minutes := timeMod(minutes, 60)
	days, hours := timeMod(hours, 24)
	years, days := timeMod(days, 365)

	return stringBuilder(years, days, hours, minutes, seconds)
}

//returns mod of time passed to it (dirty hack)
func timeMod(number float64, divider float64) (times float64, remaing float64) {
	times = float64(int(number / divider))
	remaing = math.Mod(number, divider)
	return
}

//returns full ISO8601 string
func stringBuilder(years float64, days float64, hours float64, minutes float64, seconds float64) (iso8601 string) {
	iso8601 = "P"
	if years > 0.0 {
		iso8601 = iso8601 + strconv.Itoa(int(years)) + "Y"
	}

	if days > 0.0 {
		iso8601 = iso8601 + strconv.Itoa(int(days)) + "D"
	}

	iso8601 = iso8601 + "T"

	if hours > 0.0 {
		iso8601 = iso8601 + strconv.Itoa(int(hours)) + "H"
	}
	if minutes > 0.0 {
		iso8601 = iso8601 + strconv.Itoa(int(minutes)) + "M"
	}
	if seconds > 0.0 {
		iso8601 = iso8601 + strconv.Itoa(int(seconds)) + "S"
	}
	return
}
