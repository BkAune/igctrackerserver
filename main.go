package main

import (
	"net/http"
	"os"
)

//main function of program
func main() {
	Init()
	http.HandleFunc("/", NotFoundHandler)
	http.HandleFunc("/igcinfo/api/", IgcHandler)
	http.ListenAndServe(":"+os.Getenv("PORT"), nil)
}
