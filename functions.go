package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"strconv"
	"strings"
)

//TrackInfo stores all submitted urls
var TrackInfo map[int]string

//APIInfoResponse used to format api info response
type APIInfoResponse struct {
	Duration string `json:"duration"`
	Info     string `json:"info"`
	Version  string `json:"version"`
}

//RegisterIgcStruct used to parse register url from json
type RegisterIgcStruct struct {
	URL string `json:"url"`
}

//IDResponse contains id for responses
type IDResponse struct {
	ID int `json:"id"`
}

//IDSliceResp contains slice of ids for response
type IDSliceResp struct {
	ID []int `json:"ids"`
}

//TrackInfoResp contains info for api call response
type TrackInfoResp struct {
	Date        string  `json:"h_date"`
	Pilot       string  `json:"pilot"`
	Glider      string  `json:"glider"`
	GliderID    string  `json:"glider_id"`
	TrackLenght float64 `json:"track_length"`
}

//Init starts internal server clock and map
func Init() {
	Start()
	TrackInfo = make(map[int]string)
}

//ValidURL returns valid base urls"
func ValidURL() []string {
	return []string{"/igcinfo/api/", "/igcinfo/api/igc/"}
}

//ValidURLSlice returns string slice of valid url
func ValidURLSlice() []string {
	return deleteEmpty(strings.Split(ValidURL()[1], "/"))
}

//ValidField returns string slice of valid fields
func ValidField() []string {
	return []string{"h_date", "pilot", "glider", "glider_id", "track_length"}
}

//APIInfo returns json for API info
func APIInfo(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	apiInfo := APIInfoFill()
	json.NewEncoder(w).Encode(apiInfo)
}

//APIInfoFill returns ApiInfo struct with rifht information
func APIInfoFill() (apiInfo APIInfoResponse) {
	apiInfo.Info = "Service for IGC tracks."
	apiInfo.Version = "v1"
	apiInfo.Duration = ISO8601()
	return
}

//URLIgcLen takes param URL, returns true when lenght of request is rights
func URLIgcLen(URL string) bool {
	if len(strings.Split(URL, "/")) == 5 {
		return true
	}
	return false
}

//RegisterIgcURL registers igc file url to internal memmory
func RegisterIgcURL(w http.ResponseWriter, r *http.Request) {
	var URL RegisterIgcStruct
	err := json.NewDecoder(r.Body).Decode(&URL)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		_, err := igc.ParseLocation(URL.URL)
		if err == nil {
			var id IDResponse
			id.ID = len(TrackInfo)
			TrackInfo[id.ID] = URL.URL
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(id)
		} else {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		}
	}
}

//GetTrackInfo replies with slice of all map keys
func GetTrackInfo(w http.ResponseWriter) {
	var ids IDSliceResp
	for i := range TrackInfo {
		ids.ID = append(ids.ID, i)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ids)
}

//APICallWithID takes @param URL returns true when it is of right lenght and contains valid id field
func APICallWithID(URL string) bool {
	urlSlice := deleteEmpty(strings.Split(URL, "/"))
	validURLSlice := ValidURLSlice()
	if len(urlSlice) == 4 && urlSlice[0] == validURLSlice[0] && urlSlice[1] == validURLSlice[1] && urlSlice[2] == validURLSlice[2] {
		_, err := strconv.Atoi(urlSlice[3])
		if err == nil {
			return true
		}
	}
	return false
}

//APICallWithField takes param URL returns true if ti contains right fields in request
func APICallWithField(URL string) bool {
	urlSlice := deleteEmpty(strings.Split(URL, "/"))
	validURLSlice := ValidURLSlice()
	if len(urlSlice) == 5 && urlSlice[0] == validURLSlice[0] && urlSlice[1] == validURLSlice[1] && urlSlice[2] == validURLSlice[2] && contains(ValidField(), urlSlice[4]) {
		_, err := strconv.Atoi(urlSlice[3])
		if err == nil {
			return true
		}
	}
	return false
}

//GetGlideInfo replies with info about track from internal memmory.
func GetGlideInfo(w http.ResponseWriter, r *http.Request) {
	trackID, ok := strconv.Atoi(deleteEmpty(strings.Split(r.URL.String(), "/"))[3])
	if ok != nil {
		track, err := igc.ParseLocation(TrackInfo[trackID])
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			var info TrackInfoResp
			info.Date = track.Date.String()
			info.Pilot = track.Pilot
			info.Glider = track.GliderType
			info.GliderID = track.GliderID
			info.TrackLenght = track.Task.Distance()
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(info)
		}
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}

//GetFieldInfo replies with information from specified field
func GetFieldInfo(w http.ResponseWriter, r *http.Request) {
	trackID, _ := strconv.Atoi(deleteEmpty(strings.Split(r.URL.String(), "/"))[3])
	urlString, err := TrackInfo[trackID]
	track, _ := igc.ParseLocation(urlString)
	if err == true {
		switch field := deleteEmpty(strings.Split(r.URL.String(), "/"))[4]; field {
		case ValidField()[0]:
			fmt.Fprintln(w, track.Date.String())
		case ValidField()[1]:
			fmt.Fprintln(w, track.Pilot)
		case ValidField()[2]:
			fmt.Fprintln(w, track.GliderType)
		case ValidField()[3]:
			fmt.Fprintln(w, track.GliderID)
		case ValidField()[4]:
			fmt.Fprintln(w, strconv.FormatFloat(track.Task.Distance(), 'E', -1, 64))
		default:
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}

//hepler_function
//Borrowed from http://dabase.com/e/15006/ Removes empty strings in slices
func deleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

//helper_function
//contains takes param s and e, returns true when e is contained in s
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
