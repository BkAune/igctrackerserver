package main

import (
	"net/http"
)

//NotFoundHandler runs all calls that do not follow API specification
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
}

//IgcHandler takes all calls that follow API specification
func IgcHandler(w http.ResponseWriter, r *http.Request) {
	valid := ValidURL()
	if r.URL.String() == valid[0] {
		APIInfo(w)
	} else if r.URL.String() == valid[1] {
		if URLIgcLen(r.URL.String()) && r.Method == http.MethodPost {
			RegisterIgcURL(w, r)
		} else if URLIgcLen(r.URL.String()) && r.Method == http.MethodGet {
			GetTrackInfo(w)
		}
	} else if APICallWithID(r.URL.String()) == true {
		GetGlideInfo(w, r)
	} else if APICallWithField(r.URL.String()) == true {
		GetFieldInfo(w, r)
	} else {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}
}
